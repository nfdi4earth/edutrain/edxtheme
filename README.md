# edXTheme 4 EduTrain@NFDI4Earth

``` !!! work in progress !!! ```

Version: v0.2


## Introduction

As we use [\<tutor\>](https://docs.tutor.overhang.io/index.html) as Docker-based Open edX distribitution, all further descriptions on (how to apply & develop) this theme for the NFDI4Earth instance of [edX](https://www.edx.org).

## Install the theme

### Clone project to theme-folder

```bash
git clone https://git.rwth-aachen.de/nfdi4earth/edutrain/edxtheme.git "$(tutor config printroot)/env/build/openedx/themes/edutrain"
```

### Apply theme

...this includes the compilation of the theme (see "Add theme to edX" below)

```bash
tutor images build openedx
tutor local stop lms && tutor local start lms -d
tutor local do settheme edutrain
```

_So far it is only necessary, to recreate the LMS as the theme only effects that._

### Update theme

> Note: I was struggling a lot, when I did this for the first time. As I never got the latest version of the theme!

These are the steps that need to be done, to update the theme:

```bash
# navigate to directory, where this theme is stored
cd $(tutor config printroot)/env/build/openedx/themes/edutrain
# pull updates
git pull
# re-build image of openedx
tutor images build openedx
# stop lms
tutor local stop lms
# remove container of lms
# this is essential, as tutor does not take the latest image automatically!!!
docker container rm tutor_local-lms-1
tutor local start lms -d
```

Check if the lastly build openedx image is used (e.g. via Portainer)!


## Develop the theme

Tutor gives some very basic instructions on how to develop a new theme: [Developing a new theme](https://docs.tutor.overhang.io/tutorials/theming.html#developing-a-new-theme)

This basic theme is based on the [indigo-theme](https://github.com/overhangio/tutor-indigo) by cloning the [fundamental structure of the theme](https://github.com/overhangio/tutor-indigo/tree/master/tutorindigo/templates/indigo) without using the plugin infrastructure.

To develop the theme, it is recommended _(but not obligatory)_ to do the following steps:

### Build development environment

```bash
python3 -m venv {/local/virtual/environment}
. {/local/virtual/environment}/bin/activate
pip install tutor
```

### Install development version of edX

_Inspired by documentation: [Open edX development](https://docs.tutor.overhang.io/dev.html#open-edx-development)_


```bash
tutor dev launch
```

### Add theme to edX

```bash
# copy theme to edx-project folder
cp -r {this/project} "$(tutor config printroot)/env/build/openedx/themes/edutrain"
# start lms ... this is essential, as it also compiles the theme
tutor dev start lms --skip-build -d
# configure theme to be active
tutor dev do settheme edutrain
# start watchthemes ... also essential, as it compiles the theme on each change
tutor dev run watchthemes
```

Notes:

- changing *.html-files are not always immediately visible by browser reload
    - there is a kind of caching for min. 3 minutes
    - sometimes it helps to stop and start the lms
    - but mostly, only waiting helps
- changing *.scss-files triggers a recompilation via 'watchthemes', which means you have to wait a moment

## Known Issues

### Unable to enter cms from lms

From time to time (I have not found the trigger), it happens that one is not able to access the cms anymore, when clicking on "View course in: Studio".

The only thing that helps (so far), is to relaunch the setup:

```bash
tutor local launch -I
```
